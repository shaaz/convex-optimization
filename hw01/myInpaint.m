function X = myInpaint (Xh,Omega)
  m = length(Omega);
  sz = size(Xh);
  n = sz(1);
  
  In2 = speye(n^2);
  S = In2(Omega,:);
  D = sparse([1:n-1,1:n-1],[1:n-1,2:n],[ones(1,n-1),-1*ones(1,n-1)],n-1,n);
  In = In2(1:n,1:n);
  E = [kron(In,D);kron(D,In)];
  
  I_A = speye(2*n*(n-1));
  O_A = sparse(m,2*n*(n-1));
  e = 1 + O_A(1,:)';
  f = [0*In2(:,1);e;e];
  A = [E, -1*I_A ,I_A; S, O_A, O_A];
  xh = Xh(:);
  b = [sparse(2*n*(n-1),1) ; xh(Omega)];
  
  options =optimoptions('linprog','Algorithm','interior-point','Display','none','MaxIterations',15,'OptimalityTolerance',1e-05,'ConstraintTolerance',1e-01);
%  use below for older version of matlab
%  options =optimoptions('linprog','Algorithm','interior-point','Display','none','MaxIter',15,'TolFun',1e-05);

  z = linprog(f,[],[],A,b,sparse(n^2 + 4*n*(n-1),1),[],[],options);
  X = reshape(z(1:n^2,1),n,n);
end
