function x = myL1reg1(A, b, D)
   sz    = size(A);
   mu    = 0.1;
   sigm2 = 0.156^2;
   alpha = 1;   % alpha, for line search
   beta  = 0.8;   % beta, for line search
   tol   = 1e-8;  % sub-optimality tolerance
   tolg  = 1;     % sub-optimality gradient norm tolerance
   At    = A';    % A transpose
   Dt    = D';    % D transpose
   AtA   = mu*At*A;
   Atb   = mu*At*b;
   f     = @(x) (sum( sqrt( ( (D*x).^2)+ sigm2 ) ) + (mu/2)*norm(A*x - b)^2);        % evaluate objective at x
%   gradf = @(x) (mu*At*(A*x-b) + ( Dt * ( (D*x) ./ sqrt( sigm2 + ((D*x).^2) ) ) ) ); % gradient of objective at x

   function [f,g] = f_and_grad(x)                                                    % evaluate objective and gradient simultaneouly,
       Dx = D*x;                                                                     % may save time
       a = sqrt( Dx.^2 + sigm2 );
       f = sum(a) + (mu/2)*norm(A*x-b)^2;
       g = (AtA*x -Atb) + (Dt*(Dx./a));
   end
 
   function t = line_search(x,t,f_x,grad_x)                                           % backtracking line search
       norm_gradx = alpha*norm(grad_x)^2;
       while (f(x-t*grad_x)-f_x > t*(norm_gradx))
           t = beta*t;
      end
   end

   function x_k = gradient_descent_bb(x_k_1,f_k_1,grad_k_1,x_k,f_k,grad_k)            % gradient descent with BB step
       while ( ( abs(f_k_1-f_k)/f_k_1 > tol ) || (norm(grad_k)>tolg) )
           y_k_1 = grad_k - grad_k_1;
           s_k_1 = x_k - x_k_1;
           t_k   = line_search(x_k,(y_k_1'*s_k_1)/(norm(y_k_1)^2),f_k,grad_k);        % step size at iteration k
           x_k_1 = x_k;
           grad_k_1 = grad_k;
           f_k_1 = f_k;
           x_k = x_k - t_k*grad_k;
           [f_k,grad_k] = f_and_grad(x_k);
      end
   end

   x0    = sparse(1,1,1,sz(2),1);                             % x , intital value
   [f0,grad0] = f_and_grad(x0);
   t0    = line_search(x0,1,f0,grad0);                        % line search for first step
   x     = x0-t0*grad0;
   [f_x,grad_x] = f_and_grad(x);
   x     = gradient_descent_bb(x0,f0,grad0,x,f_x,grad_x);     % gradient descent with BB step
end