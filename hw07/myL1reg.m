function x = myL1reg(A, b, D)
   sz = size(A);
   m  = sz(1);
   n  = sz(2);
   szD = size(D);
   mD = szD(1);
   nD = szD(2);
   I   = speye(mD);
   f   = sparse([zeros(n,1);ones(mD,1)]);
   Ane = sparse([D -I;-D -I]);
   bne = sparse(zeros(2*mD,1));
   Aeq = sparse([A zeros(m,mD)]);
   beq = sparse(b);
   options = optimoptions('linprog','Algorithm','interior-point','Display','none','MaxIterations',15,'OptimalityTolerance',1e-05,'ConstraintTolerance',1e-01);
   % use below for older version of matlab
   % options =optimoptions('linprog','Algorithm','interior-point','Display','none','MaxIter',15,'TolFun',1e-05);
   xt = linprog(f,Ane,bne,Aeq,beq,[],[],[],options);
   x = xt(1:n);
end
  
