% test L1 regularization for compressive sensing:
%        min ||Lx||_1, st. Ax = b  or
% min sum(sqrt((Lx).^2 + sigma)) + mu/2||Ax-b||^2

clear, close all
rng(2)

sw = input('switch (0 or 1) = ');
if isempty(sw), sw = 0; end
S = {'myL1reg','yzL1reg'};
if sw, S = {'myL1reg1','yzL1reg1'}; end

m = 150;
n = 2*m;
r = 4;

k = ceil(m/r);
A = randn(m,n);

snr = 50;
noise = randn(m,1);
noise = 10^(-snr/20)/norm(noise)*noise;

range = 50;
p = randperm(n);
xo = zeros(n,1);
xo(p(1:k))= range*randn(k,1);

D = cell(3,1);
e = ones(n,1);
D{1} = spdiags(e,0,n,n);
D{2} = spdiags([-e e],-1:0,n-1,n);
D{3} = spdiags([-e 2*e -e],-1:1,n-2,n);
eqs = '=================';
warning off

E = zeros(2,3);
T = zeros(2,3);

for i = 1:2
    solver = S{i};
    if ~exist(solver,'file'), continue, end
    x0 = xo; figure(i)
    for p = 1:3
        b = A*x0;
        b = b + norm(b)*noise;
        t0 = tic;
        x = eval([solver '(A,b,D{p})']);
        t1 = toc(t0); 
        relerr = norm(x-x0)/norm(x0);
        T(i,p) = t1; E(i,p) = relerr;
        if p==1, fprintf(['\n' eqs solver eqs '\n']); end
        fprintf('p = %1i: rel_err: %e  time: %f\n',p,relerr,t1)
        subplot(310+p); plot(1:n,x0,'b-',1:n,x,'r.'); 
        axis auto; shg
        title([solver ' Results'])
        x0 = cumsum(x0);
    end
end

rt = T(1,:)./T(2,:);
re = E(1,:)./E(2,:);
fprintf('\n ***** error and time ratios ******\n\n');
disp([re; rt])
fprintf(' **********************************\n')