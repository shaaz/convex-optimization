function [x,y,z,iter] = my_pdipm(A,b,c,tol,maxit,prt)
% INPUT:
% A = constraint coefficient matrix
% b = constraint right-hand side vector
% c = objective coefficient vector
% tol = tolerance
% maxit = maximum number of iterations allowed
% prt = switch for screen printout (1 on, 0 off)
% OUTPUT:
% x = computed primal solution
% y = computed dual solution
% z = computed dual slacks
% iter = iteration counter

  A      = sparse(A);
  b      = sparse(b);
  c      = sparse(c);
  m      = numel(b);
  n      = numel(c);
  At     = A';
  p      = symamd(abs(A)*abs(A)');
  b_norm = 1 + norm(b);
  c_norm = 1 + norm(c);

  % initial values of x,y,z
  x  = n*ones(n,1);
  z  = ones(n,1);
  y  = zeros(m,1);
  dy1= zeros(m,1);
  dy2= zeros(m,1);

  % solve RR' = rhs    ; where R is lower triangular
  solve_chol = @(R,rhs) ( R \ (R' \ rhs));

  % Predictor Corrector Algorithm
  for iter=1:maxit
    xz  = x.*z;
    xtz = sum(xz)/n;
    x_z = x./z;
    mu  = 0;
    rd  = c - At*y - z;
    rp  = b - A*x;
    rc  = mu - xz;

    % stopping criteria
    rel_primal_norm = norm(rp)/b_norm;
    rel_dual_norm   = norm(rd)/c_norm;
    by              = b'*y;
    rel_dual_gap    = norm(rc,1) / (1 + abs(by));

    if(prt == 1)
      fprintf('iter %2d:  [primal dual gap] = [%0.2e %0.2e %0.2e]\n',iter,rel_primal_norm,rel_dual_norm,rel_dual_gap);
    end

    if(rel_primal_norm + rel_dual_norm + rel_dual_gap < tol)
      break;
    end
    
    % predictor step
    M        = A*spdiags(x_z(:),0,n,n)*At;
    epsI     = 4*eps*speye(m);
    R        = chol(M(p,p)+ epsI);
    rc_z     = rc./z;
    rhs      = A*(x_z.*rd - rc_z) + rp;
    dy1(p)   = solve_chol(R,rhs(p));
    dz1      = rd - At*dy1;
    dx1      = rc_z - x_z.*dz1;
    
    % corrector step
    alpha_p = -1 / min([dx1./x;-1]);
    alpha_d = -1 / min([dz1./z;-1]);
    sigma   = (dot(x+alpha_p*dx1,z+alpha_d*dz1)/sum(xz))^2;
    mu      = sigma*xtz;
    rc      = mu - (dx1.*dz1);
    rc_z    = rc./z;
    rhs     = A*(-rc_z);
    dy2(p)  = solve_chol(R,rhs(p));
    dz2     = -At*dy2;
    dx2     = rc_z - x_z.*dz2;

    % combined step
    dx      = dx1 + dx2;
    dy      = dy1 + dy2;
    dz      = dz1 + dz2;
    alpha_p = -1 / min([dx./x;-1]);
    alpha_d = -1 / min([dz./z;-1]);

    tau     = max([0.995, 1 - 10*xtz]);
    x       = x + min([tau*alpha_p,1])*dx;
    dyz     = min([tau*alpha_d,1]);
    y       = y + dyz * dy;
    z       = z + dyz * dz;
  end
