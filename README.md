# README #

Contains all code developed for CAAM565 Fall2016.

* Gradient descent algorithm for convex optimization
* Primal dual interior point method for Linear Programming.
* Use CVX to solve semi-definite programs.