function [X, y, Z, out] = my_cvxsdp(A,b,C)
  % Input:
  % A: n by m*n (A = [A1 A2 ... Am])
  % b: m by 1
  % C: n by n
  % Output:
  % X: n by n, primal solution
  % y: m by 1, dual solution
  % Z: n by n, dual slack
  % out: structure for output quantities from cvx
  % (cputime, optval, status, ..., etc.)

  m = numel(b);
  n = size(C,1);
  A = sparse(A);
  C = sparse(C);
  C = C(:)';

  cvx_begin quiet SDP
     cvx_solver sdpt3
     variables X(n,n)
     dual variables y{m}
     dual variables Z
  
     minimize( C*X(:) );
     subject to
     for i = 0:m-1,
       reshape(A(:,1+i*n:(i+1)*n),[1 n^2])* X(:)  == b(i+1) : y {i+1};
     end
     X >= 0 : Z;
  cvx_end

  y = cell2mat(y);
  out = struct('cputime',cvx_cputime, 'optbnd',cvx_optbnd, 'optval',cvx_optval, 'slvitr',cvx_slvitr, 'slvtol',cvx_slvtol, 'status',cvx_status);
